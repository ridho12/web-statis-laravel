<!DOCTYPE html>
<html>
<head>
    <title>Tugas 1</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method = "POST">
        @csrf
        <label for="namaDepan">Fist Name:</label>
        <br><br>
        <input type="text" id ="namaDepan" name ="nama_depan">
        <br><br>
        <label for="namaBelakang">Last Name:</label>
        <br><br>
        <input type="text" id ="namaBelakang" name ="nama_belakang">
        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>
        <p>Nationality:</p>
        <select name="Nationality" id="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="English">English</option>
            <option value="Other">Other</option>
        </select>
        <p>Laguage Spoken:</p>
        <input type="checkbox" name="Indonesia" id="Indonesia">
        <label for="Indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="English" id="English">
        <label for="English">English</label><br>
        <input type="checkbox" name="Arabic" id="Arabic">
        <label for="Arabic">Arabic</label><br>
        <input type="checkbox" name="Japanese" id="Japanese">
        <label for="Japanese">Japanese</label><br>
        <input type="checkbox" name="other" id="other">
        <label for="other">Other</label>
        <p>Bio:</p>
        <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>